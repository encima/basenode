'use strict';

var express = require('express');
var http = require('http');
var path = require('path');
var async = require('async');
var sass = require('node-sass');
var bodyParser = require('body-parser');

// init express
var app = express();

app.set('port', process.env.PORT || 3000);


// var allowCrossDomain = function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

//     // intercept OPTIONS method
//     if ('OPTIONS' == req.method) {
//         res.send(200);
//     } else {
//         next();
//     }
// };
// app.use(allowCrossDomain);


// app.use(bodyParser.urlencoded({
//     extended: true
// }));
// app.use(bodyParser.json());

// set logging
app.use(function(req, res, next) {
    console.log('%s %s', req.method, req.url);
    next();
});

app.use(sass.middleware({
    src: __dirname + '/sass',
    dest: __dirname + '/public/css',
    prefix: 'public/css',
    debug: true,
    outputStyle: 'compressed'
}));

// mount static
// app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname));

// start server
http.createServer(app).listen(app.get('port'), function() {
    console.log('App started on ' + app.get('port'));
});